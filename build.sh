#!/bin/bash
####################################################################################
#  Copyright 2021 ModalAI Inc.
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
# 
#  1. Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
# 
#  2. Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
# 
#  3. Neither the name of the copyright holder nor the names of its contributors
#     may be used to endorse or promote products derived from this software
#     without specific prior written permission.
# 
#  4. The Software is used solely in conjunction with devices provided by
#     ModalAI Inc.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.
####################################################################################

################################################################################
# Downloads an build Boost libraries for the VOXL platform
################################################################################

set -e -x

# For fancy mv and stuff
shopt -s extglob

# Create a directory to work in
mkdir -p boost
cd boost

# the Boost version we want to use
BOOST_VERSION=1_65_0
BOOST_VERSION_DOTS=1.65.0

# Some variables that are nice to have
BOOST_SRC_TAR="boost_${BOOST_VERSION}.tar.gz"
BOOST_SRC_DIR="boost_${BOOST_VERSION}"

# Down
if [ ! -f "$BOOST_SRC_TAR" ]; then
	wget https://boostorg.jfrog.io/artifactory/main/release/${BOOST_VERSION_DOTS}/source/${BOOST_SRC_TAR}
fi

# Remove any old boost directories that we dont want anymore
mkdir -p files_to_get_rid_of
mv  !(files_to_get_rid_of) files_to_get_rid_of
mv files_to_get_rid_of/${BOOST_SRC_TAR} .
rm -rf files_to_get_rid_of

# Extract the binary
tar -xf ${BOOST_SRC_TAR}

# Create the directory for boost to place all the build output
rm -rf build64
mkdir -p build64

# Do everything else from the boost dir
cd ${BOOST_SRC_DIR}

# Bootstrap the build system
./bootstrap.sh --includedir=./../build64/usr/include/boost64 --libdir=./../build64/usr/lib64 --without-libraries=python,mpi

# Replace the toolchain used in the project config with the arm one
# sed -i 's/using gcc/using gcc : arm : aarch64-linux-gnu-g++/' ./project-config.jam 
sed -i 's/using gcc/using gcc : arm : aarch64-linux-gnu-g++-4.9/' ./project-config.jam

# BUILD!!!
./b2 install toolset=gcc-arm address-model=64 architecture=arm binary-format=elf abi=aapcs threading=multi toolset=gcc
